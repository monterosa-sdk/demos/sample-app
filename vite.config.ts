import normalizeUrl from "normalize-url";
import { defineConfig, loadEnv } from "vite";
import { viteStaticCopy } from "vite-plugin-static-copy";

const env = loadEnv("all", process.cwd());

export default defineConfig({
  base: env.VITE_BASEPATH,
  plugins: [
    viteStaticCopy({
      targets: [
        {
          src: ["spec/spec.json"],
          dest: "spec",
          transform: (contents) => {
            let transformed = contents
              .toString()
              .replace(
                /__VITE_PUBLIC_HOST__/g,
                env.VITE_PUBLIC_HOST || "http://localhost:5173/"
              )
              .replace(/__VITE_BASEPATH__/g, env.VITE_BASEPATH || "");

            const json = JSON.parse(transformed);

            [
              "base_apps_url",
              "dash_image",
              "fields",
              "elements",
              "embed_url",
              "project_settings",
            ].forEach((key) => {
              json[key] = normalizeUrl(json[key]);
            });

            return JSON.stringify(json);
          },
        },
        {
          src: ["spec/**/*", "!spec/spec.json"],
          dest: "spec",
        },
      ],
    }),
  ],
});
