export function renderCallout(text, type = "info") {
  return `
    <div class="callout callout--${type}">
      <p class="callout__text">${text}</p>
    </div>
  `;
}
