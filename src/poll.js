/**
 * @file This file contains the implementation of a poll component.
 * The poll component renders a question and a set of answer options,
 * allowing users to select an answer and view the results.
 * It also provides functions for setting up and tearing down the poll component.
 */

import {
  answer,
  onElementUpdated,
  onElementResults,
} from "@monterosa-sdk/interact-kit";
import cxs from "classnames";

// Array to store unsubscribe functions
const unsubs = [];

/**
 * Renders the poll component based on the provided poll object.
 */
function renderPoll(poll) {
  const { question, answerOptions, results, hasBeenAnswered, userAnswer } =
    poll;

  const answerIndex = hasBeenAnswered ? userAnswer.answers[0].option : null;

  return `
    <div class="poll" id="poll">
      <h2 class="poll__question">${question.text}</h2>
      <div class="poll__options">
        ${answerOptions
          .map(
            (option, idx) => `
              <div class="poll__option">
                <button class="${cxs("poll__button", {
                  "poll__button--selected": idx === answerIndex,
                })}" ${hasBeenAnswered ? "disabled" : " "}>
                  <span class="poll__spacer"></span>
                  <span class="poll__text">${option.text}</span>
                  <span class="poll__percent">
                    ${results !== null ? results[idx].percentage + "%" : ""}
                  </span>
                  <span class="poll__filler" style="width: ${
                    results !== null ? results[idx].percentage : 0
                  }%" />
                </button>
              </div>
            `
          )
          .join("")}
        </div>
    </div>
  `;
}

/**
 * Sets up event listeners and subscriptions for the poll component.
 */
function setupPoll(poll, event) {
  const buttons = document.querySelectorAll(".poll__button");

  buttons.forEach((button, idx) => {
    button.addEventListener("click", () => {
      answer(poll, idx);
      renderAndSetupPoll(poll, event);
    });
  });

  // Subscribe to element updated events
  const updateUnsub = onElementUpdated(event, () => {
    renderAndSetupPoll(poll, event);
  });

  // Subscribe to element results events
  const resultsUnsub = onElementResults(poll, () => {
    renderAndSetupPoll(poll, event);
  });

  // Store the unsubscribe functions in a list (unsubs) for cleanup later
  unsubs.push(updateUnsub);
  unsubs.push(resultsUnsub);
}

/**
 * Tears down event listeners and subscriptions for the poll component.
 */
export function teardownPoll() {
  unsubs.forEach((unsub) => unsub());
}

/**
 * Renders and sets up the poll component based on the provided poll object and event name.
 */
export function renderAndSetupPoll(poll, event) {
  teardownPoll();

  document.querySelector("#app").innerHTML = renderPoll(poll);

  setupPoll(poll, event);
}
