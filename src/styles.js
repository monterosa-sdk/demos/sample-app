/**
 * Initialises styles based on the provided fields. It processes fields to
 * import CSS files and define CSS variables, then injects them into the document.
 *
 * @param {Object} fields - An object containing style-related fields. Keys should
 * be in the format "css_<role>_<name>".
 *   - The 'role' can be "import" for CSS imports or "variable" for CSS variables.
 *   - The 'name' is the specific name of the CSS variable.
 */
export function initStyles(fields) {
  const imports = [];
  const variables = [];

  // Process each field to separate imports and variables
  Object.entries(fields).forEach(([key, value]) => {
    const [type, role, name] = key.split("_");

    if (type !== "css") {
      return;
    }

    // Handle different roles
    switch (role) {
      case "import":
        imports.push(value);
        break;
      case "variable":
        variables.push([name, value]);
        break;
    }
  });

  // Inject the processed imports and variables into the document
  injectStyleImports(imports);
  injectStyleVariables(variables);
}

/**
 * Injects CSS variables into the document by creating a <style> element with the variables
 * defined in the :root selector.
 *
 * @param {Array} variables - An array of [name, value] pairs representing CSS variables.
 */
function injectStyleVariables(variables) {
  const style = document.createElement("style");

  style.innerHTML = `
    :root {
      ${variables.map(([key, value]) => `--${key}: ${value};`).join("\n")}
    }
  `;

  document.head.appendChild(style);
}

/**
 * Injects CSS imports into the document by creating a <style> element with the @import
 * rules for each provided URL.
 *
 * @param {Array} imports - An array of URLs to be imported as CSS files.
 */
function injectStyleImports(imports) {
  const style = document.createElement("style");

  style.innerHTML = imports.map((value) => `@import url(${value});`).join("\n");

  document.head.appendChild(style);
}
