/**
 * This file is the main entry point of the sample app.
 * It imports necessary modules, sets up the configuration,
 * and initialises the app based on the provided query parameters.
 * The app retrieves connection health, event details, and elements,
 * and renders a poll if found, or displays a callout message if no poll is found.
 * It also handles element publishing, revoking, and connection health state changes.
 */

import { configure } from "@monterosa-sdk/core";
import {
  getEvent,
  getProject,
  getElements,
  getConnectionHealth,
  onElementPublished,
  onElementRevoked,
  onConnectionHealthState,
  ConnectionHealthState,
} from "@monterosa-sdk/interact-kit";

import { initStyles } from "./styles";
import { renderCallout } from "./callout";
import { renderAndSetupPoll } from "./poll";

import "./styles.css";

// Getting the host, project ID, and event ID from the query parameters
const url = new URL(document.location.href);
const host = url.searchParams.get("h");
const projectId = url.searchParams.get("p");
const eventId = url.searchParams.get("e");

// Getting the app element
const app = document.querySelector("#app");

/**
 * Initialises the application.
 */
async function init() {
  // Configure the SDK with the provided host and project ID
  configure({
    host,
    projectId,
  });

  // Get the connection health status
  const health = await getConnectionHealth();

  // Get the event details
  const event = await getEvent(eventId);

  // Get the project data
  const project = await getProject();

  initStyles(project.fields);

  // Find and render the poll for the event
  findAndRenderPoll(event);

  // Listen for element published events and re-render the poll
  onElementPublished(event, () => findAndRenderPoll(event));

  // Listen for element revoked events and re-render the poll
  onElementRevoked(event, () => findAndRenderPoll(event));

  // Listen for connection health state changes
  onConnectionHealthState(health, (state) => {
    switch (state) {
      case ConnectionHealthState.Ok:
        findAndRenderPoll(event);
        break;
      case ConnectionHealthState.Error:
        // Display an error callout message if the connection is lost
        app.innerHTML = renderCallout(
          "Connection to the Monterosa Studio has been lost. Please check your internet connection.",
          "error"
        );
        break;
    }
  });
}

/**
 * Finds and renders the poll for the given event.
 * If no poll is found, displays a warning callout message.
 */
async function findAndRenderPoll(event) {
  // Get the elements for the event
  const elements = await getElements(event);

  // Find the latest poll element
  const poll = elements.reverse().find((element) => element.type === "poll");

  if (poll !== undefined) {
    // Render and setup the poll
    renderAndSetupPoll(poll, event);
  } else {
    // Display a warning callout message if no poll is found
    app.innerHTML = renderCallout(
      "No poll found in this event. Please add a poll to the event.",
      "warning"
    );
  }
}

// Check if the host, project ID, and event ID are provided
if (host && projectId && eventId) {
  // Initialise the app
  init();
} else {
  // Display an error callout message if the required parameters are missing
  app.innerHTML = renderCallout(
    "Host, Project ID and Event ID are required. Please provide all three as " +
      "query parameters 'h', 'p', and 'e' respectively.",
    "error"
  );
}
